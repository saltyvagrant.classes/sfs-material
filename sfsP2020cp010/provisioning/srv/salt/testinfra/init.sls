{%- from "testinfra/map.jinja" import testinfra with context %}
{%- set packages = testinfra.pkgs %}
{%- set pip_packages = testinfra.pip %}


testinfra_packages:
  pkg.installed:
  - pkgs:
{%- for pkg in packages %}
    - {{ pkg }}
{%- endfor %}

testinfra_pip_packages:
  pip.installed:
  - pkgs:
{%- for pkg in pip_packages %}
    - {{ pkg }}
{%- endfor %}
  require:
    - pkg: testinfra_packages
