firewall:
  enabled: True
  install: True
  strict: True
  services:
{%- if grains['is_vagrant'] is defined and grains['is_vagrant'] == True %}
    ssh:
      block_nomatch: False
      interfaces:
        - eth0
{%- endif %}
    http:
      block_nomatch: False
      interfaces:
        - eth0
