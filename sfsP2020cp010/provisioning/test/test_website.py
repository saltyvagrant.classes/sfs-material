import pytest

def test_index_available(host):
    index_file = host.file("/var/www/html/index.html")
    assert index_file.exists
    assert index_file.is_file
