import pytest

@pytest.mark.parametrize("name,version", [
    ("git", "1:2.20"),
    ("tree", "1.8"),
])
def test_packages(host, name, version):
    pkg = host.package(name)
    assert pkg.is_installed
    assert pkg.version.startswith(version)
