import pytest

@pytest.mark.parametrize("name,version", [
    ("iptables", "1.8"),
    ("iptables-persistent", "1.0"),
])
def test_packages(host, name, version):
    pkg = host.package(name)
    assert pkg.is_installed
    assert pkg.version.startswith(version)


def test_INPUT_policy_drop(host):
    rules = host.iptables.rules(chain='INPUT')
    assert '-P INPUT DROP' in rules


def test_http_open(host):
    rules = host.iptables.rules(chain='INPUT')
    assert '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -j ACCEPT' in rules

@pytest.mark.vagrant
def test_ssh_open(host):
    rules = host.iptables.rules(chain='INPUT')
    assert '-A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT' in rules
